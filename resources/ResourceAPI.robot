*** Settings ***

Documentation    Documentação da API: https://fakerestapi.azurewebsites.net/index.html
Library          RequestsLibrary  
Library          Collections
Library          DateTime

*** Variables ***

${URL_API}       https://fakerestapi.azurewebsites.net/api/v1/ 

&{HEADERS}       content-type=application/json

*** Keywords ***

Conectar na API
    Create Session        fakeAPI       ${URL_API}

Requisitar todos os livros
    ${RESPONSE}           Get On Session     fakeAPI    Books   
    Log                   ${RESPONSE.text}
    Set Test Variable     ${RESPONSE}

Requisitar o livro
    [Arguments]           ${ID_BOOK}
    ${RESPONSE}           Get On Session     fakeAPI    Books/${ID_BOOK}
    Log                   ${RESPONSE.text}
    Set Test Variable     ${RESPONSE}

Cadastrar novo livro
    ${BODY}        Create Dictionary    id=203 
    ...                                 title=teste  
    ...                                 description=teste  
    ...                                 pageCount=200  
    ...                                 excerpt=teste  
    ...                                 publishDate=2021-05-02T17:15:25.598Z

    ${RESPONSE}    Post On Session      fakeAPI    Books
    ...                                 json=${BODY}
    ...                                 headers=${HEADERS}

    Log                                 ${RESPONSE.text}
    Set Test Variable                   ${RESPONSE}
    Set Test Variable                   ${BODY}

Requisitar alteração do livro
    [Arguments]    ${ID_BOOK}

    ${BODY}        Create Dictionary    id=${ID_BOOK} 
    ...                                 title=teste  
    ...                                 description=teste  
    ...                                 pageCount=200  
    ...                                 excerpt=teste  
    ...                                 publishDate=2021-05-02T17:15:25.598Z

    ${RESPONSE}    PUT On Session       fakeAPI    Books/${ID_BOOK}
    ...                                 json=${BODY}
    ...                                 headers=${HEADERS}

    Log                                 ${RESPONSE.text}
    Set Test Variable                   ${RESPONSE}
    Set Test Variable                   ${BODY}

Remover o livro
    [Arguments]    ${ID_BOOK}
    
    ${RESPONSE}    DELETE On Session    fakeAPI    Books/${ID_BOOK}

    Set Test Variable                   ${RESPONSE}

Conferir o status code
    [Arguments]                         ${STATUS}
    Should Be Equal As Strings          ${STATUS}    ${RESPONSE.status_code}

Conferir o reason
    [Arguments]                         ${STATUS}
    Should Be Equal As Strings          ${STATUS}    ${RESPONSE.reason}

Conferir se retorna uma lista com "${QTD_BOOKS}" livros
    Length Should Be                    ${RESPONSE.json()}    ${QTD_BOOKS}

Conferir se retorna corretamente os dados do livro
    Dictionary Should Contain Item      ${RESPONSE.json()}    id               15
    Dictionary Should Contain Item      ${RESPONSE.json()}    title            Book 15
    Dictionary Should Contain Item      ${RESPONSE.json()}    pageCount        1500
    Should Not Be Empty                 ${RESPONSE.json()["description"]}
    Should Not Be Empty                 ${RESPONSE.json()["excerpt"]}
    Should Not Be Empty                 ${RESPONSE.json()["publishDate"]}

Conferir os dados do livro
    Dictionary Should Contain Item      ${RESPONSE.json()}    id               ${BODY.id}
    Dictionary Should Contain Item      ${RESPONSE.json()}    title            ${BODY.title}
    Dictionary Should Contain Item      ${RESPONSE.json()}    pageCount        ${BODY.pageCount}
    Dictionary Should Contain Item      ${RESPONSE.json()}    description      ${BODY.description}
    Dictionary Should Contain Item      ${RESPONSE.json()}    excerpt          ${BODY.excerpt}
    Dictionary Should Contain Item      ${RESPONSE.json()}    publishDate      ${BODY.publishDate}

