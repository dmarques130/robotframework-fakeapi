*** Settings ***

Resource         ../resources/ResourceAPI.robot 
Suite Setup      Conectar na API
  
*** Test Cases ***

Buscar a listagem de todos os livros (GET)
    Requisitar todos os livros
    Conferir o status code  200
    Conferir o reason  OK
    Conferir se retorna uma lista com "200" livros
    

Buscar um livro expecifico (GET)
    Requisitar o livro  15
    Conferir o status code  200
    Conferir o reason  OK
    Conferir se retorna corretamente os dados do livro

